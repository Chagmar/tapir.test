1. Устанавливаем NodeJS с модулями
npm i

2. Команды gulp:
gulp styles - компилирует sass единоразово
gulp watch - следить за sass файлами и компилит при изменении
gulp compress - создает минифицированный файл стилей css/styles.min.css

3. Файлы:
styles.scss - стили для тегов + сборка остальных scss.
_variable.scss - scss переменные.
_page.scss - остальные классы, обычно разделяю на elements (используются на разных страницах), header, footer, category, product и т.п.