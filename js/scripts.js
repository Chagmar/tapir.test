// Меню
var body = document.querySelector('body');
var menuShow = document.querySelector('.menu-show');
var menuClose = document.querySelector('.menu-close');
var backdrop = document.querySelector('.backdrop');

menuShow.addEventListener('click', function() {
    body.classList.add('menu-open');
});

menuClose.addEventListener('click', function() {
    body.classList.remove('menu-open');
});

backdrop.addEventListener('click', function() {
    body.classList.remove('menu-open');
});


// Работа формы
var form = document.querySelector('form');
var btn = form.querySelector('button[type="submit"]');

var card_name = form.querySelector('[name="card_name"]');
var card_cvc = form.querySelector('[name="card_cvc"]');
var card_num = form.querySelectorAll('.card-number input');

// Ввод номера карты
function deleteNonNumber(text) {
    return text.replace(/[^0-9\.]/g, '');
}
card_num.forEach(function(item, i, card_num) {
    card_num[i].addEventListener('input', function(e){
        var cleanValue = deleteNonNumber(e.target.value);
        e.target.value = cleanValue;
        if (card_num[i].value.length == 4){
            if(i < 3){
                i++;
                card_num[i].focus();
            }
        }
        if (card_num[i].value.length > 4){
            card_num[i].value = card_num[i].value.slice(1);
        }
    });
});

// Только латинские символы в ФИО
function deleteNonLatin(text) {
    return text.replace(/[^A-Za-z ]/ig, '');
}
card_name.addEventListener('input', function(e){
    var cleanValue = deleteNonLatin(e.target.value);
    e.target.value = cleanValue;
});

// Проверка CVC
card_cvc.addEventListener('input', function(e){
    var cleanValue = deleteNonNumber(e.target.value);
    e.target.value = cleanValue;
    if (card_cvc.value.length > 3){
        card_cvc.value = card_cvc.value.slice(1);
    } 
});

// Валидация полей
function cardNumValidation(){
    card_num.forEach(function(item, i, card_num) {
        if(card_num[i].value.length < 4){
            card_num[i].classList.add('has-error');
        } else {
            card_num[i].classList.remove('has-error');
        }
    });
}
function cardNameValidation(){
    if (card_name.value.length < 4){
        card_name.classList.add('has-error');
    } else {
        card_name.classList.remove('has-error');
    }
}
function cardCvcValidation(){
    if (card_cvc.value.length < 3){
        card_cvc.classList.add('has-error');
    } else {
        card_cvc.classList.remove('has-error');
    }
}

// Проверка полей при отправке формы
btn.addEventListener('click', function() {
    cardNumValidation();
    cardNameValidation();
    cardCvcValidation();
});

form.addEventListener('submit', function(e) {
    e.preventDefault();
    
    var error = form.querySelectorAll('.has-error');
    
    if (error.length == 0) {
        alert('Успешная отправка формы');
    } else {
        return false;
    }  
});